# patchfetch.py

A small proof of concept that iterates over a list of CVEs (it expects the format that [the list of the security-tracker-team has](https://salsa.debian.org/security-tracker-team/security-tracker/blob/master/data/CVE/list).
For every CVE the script checks the URLs in the notes and if the URLs contain patches the script downloads the patches to a temporary directory.
It uses `python3-unidiff` to detect if data is a patch.
